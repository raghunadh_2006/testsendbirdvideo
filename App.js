// @flow
import React from 'react';
import {
    StyleSheet,
    View,
    SafeAreaView,
    Text,
    NativeModules,
    TouchableOpacity
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import AsyncStorage from '@react-native-async-storage/async-storage';

const App = () => {
    const [loggedIn, setLoggedIn] = React.useState(false);

    const dial = () => {
        if (selectedValueToDial?.name) {
            NativeModules.SendBirdManager.dial(selectedValueToDial?.name)
        }
    }

    const login = async () => {
        if (selectedValue) {
            const user = JSON.stringify(selectedValue);
            await AsyncStorage.setItem('USER_KEY', user);
            NativeModules.SendBirdManager.register(selectedValue?.name, selectedValue?.accessToken)
            setLoggedIn(true);
        }
    }

    const logout = async () => {
        await AsyncStorage.removeItem('USER_KEY');
        setSelectedValue(null);
        setSelectedValueToDial(null);
        setLoggedIn(false);
    }

    const [selectedValue, setSelectedValue] = React.useState(null);

    const [selectedValueToDial, setSelectedValueToDial] = React.useState(null);

    const options = [
        {
            label: 'Test1', value: {
                name: 'Test1',
                accessToken: '5a880bb7da2e2428ac16939f090b728ee564515b'
            }
        },
        {
            label: 'Test2', value: {
                name: 'Test2',
                accessToken: '28cf836d32105abf2acb692322cb5447702a0382'
            }
        },
        {
            label: 'Test3', value: {
                name: 'Test3',
                accessToken: '583d63a39376057187729ad6b2d2c0d54f51e0ae'
            }
        },
        {
            label:"Test4", value:{
                name:"Test4",
                accessToken:"c8809b22550c4156363a3d10b2710e647515f7cd"
            }
        },
        {
            label: 'Test5', value: {
                name: 'Test5',
                accessToken: '0957bb20446b83636f180e7490d1b75418de5eb0'
            }
        }
    ]

    React.useEffect(() => {
        AsyncStorage.getItem('USER_KEY').then(user => {
            if (user) {
                let userTemp = JSON.parse(user)
                setSelectedValue(userTemp);
                setLoggedIn(true);
                NativeModules.SendBirdManager.register(userTemp?.name, userTemp?.accessToken)
            }
        }
        )
    }, [])


    return (
        <SafeAreaView style={{ flex: 1 }}>
            {
                loggedIn ? <View style={styles.container}>
                    <Text style={styles.text}>{selectedValue?.name}</Text>
                    <Text style={styles.text}>Wait for a call or dial</Text>
                    <View style={styles.dropdown}>
                        <RNPickerSelect
                            placeholder={{ label: 'Select a user to call', value: null }}
                            onValueChange={(value) => setSelectedValueToDial(value)}
                            items={options}
                            style={styles.dropdown}
                        />
                    </View>
                    <TouchableOpacity style={styles.btn} onPress={dial}>
                        <Text style={styles.btnText}>Dial</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnLogout} onPress={logout}>
                        <Text style={[styles.btnText, { color: 'black' }]}>Logout</Text>
                    </TouchableOpacity>
                </View>
                    : <View style={[styles.container]}>
                        <View style={styles.dropdown}>
                            <RNPickerSelect
                                placeholder={{ label: 'Select a user to login', value: null }}
                                onValueChange={(value) => setSelectedValue(value)}
                                items={options}
                                style={styles.dropdown}
                            /></View>
                        <TouchableOpacity style={styles.btn} onPress={login}>
                            <Text style={styles.btnText}>Login</Text>
                        </TouchableOpacity>
                    </View>
            }
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 0,
        display: 'flex',
        justifyContent: 'center',
        backgroundColor: '#fff',
        paddingHorizontal: 40,
        flexDirection: 'column',
        flex: 1,
    },
    text: {
        color: 'black',
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
        marginBottom: 60,
    },
    btn: {
        padding: 20,
        backgroundColor: '#081F7B',
        borderRadius: 5,
        marginTop: 20,
    },
    btnText: {
        color: '#fff',
        textAlign: 'center',
        textTransform: 'uppercase',
        fontWeight: 'bold',
    },
    dropdown: {
        borderColor: '#081F7B',
        borderRadius: 10,
        borderWidth: 1,
        color: '#fff',
        marginBottom: 20,
        paddingHorizontal: 10,
        paddingVertical: 20
    },
    btnLogout: {
        padding: 20,
        marginTop: 40
    }
});

export default App;