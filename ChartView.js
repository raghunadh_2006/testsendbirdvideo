// @flow
import React, { Component } from 'react';
import { requireNativeComponent } from 'react-native';

const MODULE_NAME = 'ChartView';
const Chart = requireNativeComponent(MODULE_NAME, null);

type PropsType = {|
    +xValues: string[],
    +yValues: number[],
|};

class ChartView extends Component<PropsType> {
    render() {
        const { xValues, yValues, zValues } = this.props;

        return (
            <Chart
                xValues={xValues}
                yValues={yValues}
                zValues={zValues}
                style={{ flex: 1 }}
            />
        );
    }
}

export default ChartView;