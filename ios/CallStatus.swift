//
//  CallStatus.swift
//  Test
//
//  Created by Raghu on 06/06/22.
//

enum CallStatus {
    case connecting
    case muted(user: String)
    case ended(result: String)
    
    var message: String {
        switch self {
            case .connecting:
                return "call connecting..."
            case .muted(let user):
                return "\(user) is muted"
            case .ended(let result):
                return result
                    .replacingOccurrences(of: "_", with: " ")
                    .capitalizingFirstLetter()
        }
    }
}

