//
//  CredentialDelegate.swift
//  Test
//
//  Created by Raghu on 06/06/22.
//

protocol CredentialDelegate: AnyObject {
    /// Called when a current credential was updated.
    /// - Parameters:
    ///     - credential: An updated `Credential` object. It's optional type.
    func didUpdateCredential(_ credential: Credential?)
}
