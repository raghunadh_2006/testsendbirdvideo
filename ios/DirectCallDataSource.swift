//
//  DirectCallDataSource.swift
//  Test
//
//  Created by Raghu on 06/06/22.
//

import UIKit
import SendBirdCalls

protocol DirectCallDataSource {
    var call: DirectCall! { get set }
    
    var isDialing: Bool? { get set }
}
