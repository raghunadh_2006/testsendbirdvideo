//
//  SendBirdManager+SendBirdCallsDelegates.swift
//  Test
//
//  Created by Raghu on 07/06/22.
//

import UIKit
import CallKit
import SendBirdCalls

// MARK: - Sendbird Calls Delegates
extension SendBirdManager: SendBirdCallDelegate, DirectCallDelegate {
  
    // MARK: SendBirdCallDelegate
    // Handles incoming call. Please refer to `AppDelegate+VoIP.swift` file
    func didStartRinging(_ call: DirectCall) {
        print("didStartRinging")
        
        call.delegate = self // To receive call event through `DirectCallDelegate`
        
        guard let uuid = call.callUUID else { return }
        guard CXCallManager.shared.shouldProcessCall(for: uuid) else { return }  // Should be cross-checked with state to prevent weird event processings
        
        // Use CXProvider to report the incoming call to the system
        // Construct a CXCallUpdate describing the incoming call, including the caller.
        let name = call.caller?.userId ?? "Unknown"
        let update = CXCallUpdate()
        update.remoteHandle = CXHandle(type: .generic, value: name)
        update.hasVideo = call.isVideoCall
        update.localizedCallerName = call.caller?.userId ?? "Unknown"
        
        if SendBirdCall.getOngoingCallCount() > 1 {
            // Allow only one ongoing call.
            CXCallManager.shared.reportIncomingCall(with: uuid, update: update) { _ in
                CXCallManager.shared.endCall(for: uuid, endedAt: Date(), reason: .declined)
            }
            call.end()
        } else {
            // Report the incoming call to the system
            CXCallManager.shared.reportIncomingCall(with: uuid, update: update)
        }
    }
    
    // MARK: DirectCallDelegate
  
    func didConnect(_ call: DirectCall) {
        guard let call = SendBirdCall.getCall(forUUID: call.callUUID!) else {
            return
        }
        SendBirdCall.authenticateIfNeed { [weak call] (error) in
            guard let call = call, error == nil else {return}
            // MARK: SendBirdCalls - DirectCall.accept()
            let callOptions = CallOptions(isAudioEnabled: true, isVideoEnabled: call.isVideoCall, useFrontCamera: true)
            let acceptParams = AcceptParams(callOptions: callOptions)
            call.accept(with: acceptParams)
            UIApplication.shared.showCallController(with: call)
        }
    }
  

    func didEnd(_ call: DirectCall) {
        var callId: UUID = UUID()
        if let callUUID = call.callUUID {
            callId = callUUID
        }
        CXCallManager.shared.endCall(for: callId, endedAt: Date(), reason: call.endResult)
       // guard let callLog = call.callLog else { return }
       // UserDefaults.standard.callHistories.insert(CallHistory(callLog: callLog), at: 0)
    }
    
//    func didAudioDeviceChange(_ call: DirectCall, session: AVAudioSession, previousRoute: AVAudioSessionRouteDescription, reason: AVAudioSession.RouteChangeReason) {
//        print("[SendBirdCallDelegate] didAudioDeviceChange")
//        sendEvent(withName: Events.OnDirectCallAudioDeviceChanged, body: DataMapper.CreateAudioDeviceChangedMap(call: call, session: session, previousRoute: previousRoute, reason: reason))
//    }
    
    func didEstablish(_ call: DirectCall) {
        print("[SendBirdCallDelegate] didEstablish")
//        CXCallManager.shared.startCXCall(call) { isSuccess in
//            if isSuccess {
//              guard let uuid = call.callUUID else { return }
//              let name = call.caller?.userId ?? "Unknown"
//              let update = CXCallUpdate()
//              update.remoteHandle = CXHandle(type: .generic, value: name)
//              update.hasVideo = call.isVideoCall
//              update.localizedCallerName = call.caller?.userId ?? "Unknown"
//              CXCallManager.shared.reportIncomingCall(with: uuid, update: update)
//              UIApplication.shared.showCallController(with: call);
//            }
//        }
//        sendEvent(withName: Events.OnDirectCallEstablished, body: DataMapper.CreateDirectCallMap(call: call))
    }
    
    func didReconnect(_ call: DirectCall) {
        print("[SendBirdCallDelegate] didReconnect")
//        sendEvent(withName: Events.OnDirectCallReconnected, body: DataMapper.CreateDirectCallMap(call: call))
    }
    
    func didStartReconnecting(_ call: DirectCall) {
        print("[SendBirdCallDelegate] didStartReconnecting")
//        sendEvent(withName: Events.OnDirectCallReconnecting, body: DataMapper.CreateDirectCallMap(call: call))
    }
    
    func didRemoteAudioSettingsChange(_ call: DirectCall) {
        print("[SendBirdCallDelegate] didRemoteAudioSettingsChange")
//        sendEvent(withName: Events.OnDirectCallRemoteAudioSettingsChanged, body: DataMapper.CreateDirectCallMap(call: call))
    }
    
    func didRemoteRecordingStatusChange(_ call: DirectCall) {
        print("[SendBirdCallDelegate] didRemoteRecordingStatusChange")
//        sendEvent(withName: Events.OnDirectCallRemoteRecordingStatusChanged, body: DataMapper.CreateDirectCallMap(call: call))
    }
    
    func didRemoteVideoSettingsChange(_ call: DirectCall) {
        print("[SendBirdCallDelegate] didRemoteVideoSettingsChange")
//        sendEvent(withName: Events.OnDirectCallRemoteVideoSettingsChanged, body: DataMapper.CreateDirectCallMap(call: call))
    }
    
    func didUserHoldStatusChange(_ call: DirectCall, isLocalUser: Bool, isUserOnHold: Bool) {
        print("[SendBirdCallDelegate] didUserHoldStatusChange")
//        sendEvent(withName: Events.OnDirectCallUserHoldStatusChanged, body: DataMapper.CreateDirectCallMap(call: call))
    }
    
    
  
}

