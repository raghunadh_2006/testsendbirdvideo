//
//  SendBirdManager+SoundEffects.swift
//  Test
//
//  Created by Raghu on 07/06/22.
//

import SendBirdCalls

// MARK: DirectCall sound effects
// If you use CallKit framework, you have to set ringing sound by using `CXProviderConfiguration.ringtoneSound`. See `CXProvider+QuickStart.swift` file.
// If you use CallKit framework, you must implement `CXProviderDelegate.provider(_:didActivate:)` and `CXProviderDelegate.provider(_:didDeactivate:)`
extension SendBirdManager {
    func addDirectCallSounds() {
        // SendBirdCall.setDirectCallSound("Ringing.mp3", forKey: .ringing)
        SendBirdCall.addDirectCallSound("Dialing.mp3", forType: .dialing)
        SendBirdCall.addDirectCallSound("ConnectionLost.mp3", forType: .reconnecting)
        SendBirdCall.addDirectCallSound("ConnectionRestored.mp3", forType: .reconnected)
        
        // If you want to remove added DirectCall sounds,
        // Use `SendBirdCall.removeDirectCallSound(forType:)`
    }
}
