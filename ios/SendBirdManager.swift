//
//  SendBirdManager.swift
//  Test
//
//  Created by Raghu on 30/03/22.
//

import UIKit
import CallKit
import PushKit
import SendBirdCalls
import AVFAudio

@objc(SendBirdManager)
class SendBirdManager: NSObject {
 
  var queue: DispatchQueue = DispatchQueue(label: "com.eco.teleconsultation.dev.appdelegate")
  var voipRegistry: PKPushRegistry?
  
  @objc(register:withAccessToken:)
  func register(userId: String, accessToken: String){
      let appId = "EFBD3FAD-C763-4E79-B89D-D06D27D99CBA"
      SendBirdCall.configure(appId: appId)
      UserDefaults.standard.designatedAppId = appId
      print(userId,accessToken)
      self.signIn(with: userId, andAccessToken: accessToken)
      
//      self.autoSignIn { error in
//         if error == nil { return }
//         // Show SignIn controller when failed to auto sign in
//      }
      // To receive incoming call, you need to register VoIP push token
     self.voipRegistration()
     // addDirectCallSounds
     addDirectCallSounds()
     // To process incoming call, you need to add `SendBirdCallDelegate` and implement its protocol methods.
     SendBirdCall.addDelegate(self, identifier: "com.eco.teleconsultation.dev.delegate")
  }
  
  @objc(dial:)
  func dial(dialTo: String){
    //let dialParams = DialParams(calleeId: "sanketh", callOptions: CallOptions())
    let callOptions = CallOptions(isAudioEnabled: true, isVideoEnabled: true, useFrontCamera: true)
    let dialParams = DialParams(calleeId: dialTo, isVideoCall: true, callOptions: callOptions, customItems: [:])
    SendBirdCall.dial(with: dialParams)
  }

  //MARK: ---------------------------- to show ----------------------------
  @objc(showStoryBoardView)
  func showStoryBoardView() {
    DispatchQueue.main.async {
      let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
      let tempViewController = storyboard.instantiateViewController(withIdentifier: "TempViewController")
      if let topViewController = UIViewController.topViewController {
        topViewController.view.addSubview(tempViewController.view);
      }
    }
  }
  
  @objc static func requiresMainQueueSetup() -> Bool {
      return true
  }
  
  func autoSignIn(completionHandler: @escaping (Error?) -> Void) {
      // fetch credential
      guard let pendingCredential = UserDefaults.standard.credential else {
          DispatchQueue.main.async {
              completionHandler(CredentialErrors.empty)
          }
          return
      }
      // authenticate
      self.authenticate(with: pendingCredential, completionHandler: completionHandler)
  }
  
  func authenticate(with credential: Credential, completionHandler: @escaping (Error?) -> Void) {
      // Configure app ID before authenticate when the configured app ID is different.
      if SendBirdCall.appId != credential.appId, UserDefaults.standard.designatedAppId == nil {
          SendBirdCall.configure(appId: credential.appId)
      }
      
      // Authenticate
      let authParams = AuthenticateParams(userId: credential.userId, accessToken: credential.accessToken)
      SendBirdCall.authenticate(with: authParams) { (user, error) in
          guard user != nil else {
              // Failed
              DispatchQueue.main.async {
                  completionHandler(error ?? CredentialErrors.unknown)
              }
              return
          }
          // Succeed
          let credential = Credential(accessToken: credential.accessToken)
          let credentialManager = CredentialManager.shared
          credentialManager.updateCredential(credential)
          
          DispatchQueue.main.async {
              completionHandler(nil)
          }
      }
  }
  
  func signIn(with userID: String,andAccessToken accessToken: String) {
      let authParams = AuthenticateParams(userId: userID, accessToken: accessToken)
      SendBirdCall.authenticate(with: authParams) { (user, error) in
          guard user != nil else {
              // Failed
              // (Optional) If there is something wrong, clear all stored information except for voip push token.
              UserDefaults.standard.clear()
              return
          }
          // create credential object with updated information
          let credential = Credential(accessToken: "25aa93b3cbc781e4a441db246a28f399a476456f")
          let credentialManager = CredentialManager.shared
          credentialManager.updateCredential(credential)
          // register push token
          SendBirdCall.registerVoIPPush(token: UserDefaults.standard.voipPushToken, unique: false) { error in
              if let error = error { print(error) }
              
          }
      }
  }
  
  
}
