//
//  SendBirdModule.m
//  Test
//
//  Created by Raghu on 07/06/22.
//

#import <React/RCTViewManager.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface RCT_EXTERN_MODULE(SendBirdManager, NSObject)
RCT_EXTERN_METHOD(register:(NSString *)userId
                            withAccessToken:(NSString *)accessToken)
//RCT_EXTERN_METHOD(showStoryBoardView)
RCT_EXTERN_METHOD(dial:(NSString *)dialTo)
@end

@interface RCT_EXTERN_MODULE(VideoCallViewController, NSObject)
RCT_EXTERN_METHOD(endCall)
RCT_EXTERN_METHOD(flipCamera)
RCT_EXTERN_METHOD(videoOff)
RCT_EXTERN_METHOD(audioOff)
@end
